import { createRouter, createWebHistory } from 'vue-router'
import { defineAsyncComponent } from 'vue'
const bind = defineAsyncComponent(() => import('../views/bind.vue'))
const bind_open = defineAsyncComponent(() => import('../views/bind_open.vue'))
const main = defineAsyncComponent(() => import('../views/main.vue'))
const add_work_order = defineAsyncComponent(() => import('../views/add_work_order.vue'))
const add_work_order_noauth = defineAsyncComponent(() => import('../views/add_work_order_noauth.vue'))
const select_type = defineAsyncComponent(() => import('../views/select_type.vue'))
const select_type_noauth = defineAsyncComponent(() => import('../views/select_type_noauth.vue'))
const work_order_list = defineAsyncComponent(() => import('../views/work_order_list.vue'))
const work_order_detail = defineAsyncComponent(() => import('../views/work_order_detail.vue'))
const work_order_detail_noauth = defineAsyncComponent(() => import('../views/work_order_detail_noauth.vue'))
const analysis = defineAsyncComponent(() => import('../views/analysis.vue'))
const analysis_zaitu = defineAsyncComponent(() => import('../views/analysis_zaitu.vue'))
const analysis_team = defineAsyncComponent(() => import('../views/analysis_team.vue'))
const mine = defineAsyncComponent(() => import('../views/mine.vue'))
const help = defineAsyncComponent(() => import('../views/help.vue'))
const zhishiku = defineAsyncComponent(() => import('../views/zhishiku.vue'))
const shezhi = defineAsyncComponent(() => import('../views/shezhi.vue'))
const baozhangma = defineAsyncComponent(() => import('../views/baozhangma.vue'))
const baozhang_success = defineAsyncComponent(() => import('../views/baozhang_success.vue'))
const routerHistory = createWebHistory("/")

function getQueryString(url) {
  if(url) {
    url=url.substr(url.indexOf("?")+1); //字符串截取，比我之前的split()方法效率高
  }
  var result = {}, //创建一个对象，用于存name，和value
      queryString =url || location.search.substring(1), //location.search设置或返回从问号 (?) 开始的 URL（查询部分）。
      re = /([^&=]+)=([^&]*)/g, //正则，具体不会用
      m;

  while (m = re.exec(queryString)) { //exec()正则表达式的匹配，具体不会用
    result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]); //使用 decodeURIComponent() 对编码后的 URI 进行解码
  }

  return result;
}

// demo

const routes = [
  {
    path: '/',
    redirect: '/main',
    meta:{
      // xxx是自己想要设置的标题名
      title: '首页'
    }
  },
  {
    path: '/bind',
    component: bind,
    meta:{
      // xxx是自己想要设置的标题名
      title: '首页'
    }
  },
  {
    path: '/baozhangma',
    component: baozhangma,
    meta:{
      // xxx是自己想要设置的标题名
      title: '客户报障码'
    }
  },
  {
    path: '/baozhang_success',
    component: baozhang_success,
    meta:{
      // xxx是自己想要设置的标题名
      title: '报障成功'
    }
  },

  {
    path: '/main',
    component: main,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '首页'
    }
  },
  {
    path: '/select_type',
    component: select_type,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '选择服务项目'
    }
  },
  {
    path: '/select_type_noauth',
    component: select_type_noauth,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '自助报障'
    }
  },
  {
    path: '/shezhi',
    component: shezhi,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '团队设置'
    }
  },
  {
    path: '/add_work_order',
    component: add_work_order,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '报障'
    }
  },
  {
    path: '/add_work_order_noauth',
    component: add_work_order_noauth,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '自助报障'
    }
  },
  {
    path: '/work_order_list',
    component: work_order_list,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '工单列表'
    }
  },
  {
    path: '/mine',
    component: mine,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '我的'
    }
  },
  {
    path: '/work_order_detail',
    component: work_order_detail,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '工单详情'
    }
  },
  {
    path: '/s',
    component: work_order_detail_noauth,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '工单详情'
    }
  },
  {
    path: '/analysis',
    component: analysis,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '综合统计',
      keepAlive: true, // 需要被缓存
      isUseCache: false,
    }
  },
  {
    path: '/analysis_zaitu',
    component: analysis_zaitu,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '在途统计'
    }
  },

  {
    path: '/analysis_team',
    component: analysis_team,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '团队统计',
      keepAlive: true, // 需要被缓存
      isUseCache: false,
    }
  },
  {
    path: '/help',
    component: help,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '帮助中心'
    }
  },
  {
    path: '/zhishiku',
    component: zhishiku,
    props: true,
    meta:{
      // xxx是自己想要设置的标题名
      title: '知识库'
    }
  }
]
const router = createRouter({
  history: routerHistory,
  routes
})
router.beforeEach((to, from, next) => {

      let title = to.meta.title;
      let href = to.href;
      var myParam = getQueryString(href);
      let pageType = myParam ? myParam.pageType : '';
      let indexOfMain = href.indexOf("main");
      let indexOfBind = href.indexOf("bind");
      if (indexOfMain >= 0 || indexOfBind >= 0) {
        if (localStorage.urlRef) {
          let urlRef = JSON.parse(localStorage.urlRef);
          if (urlRef) {
            let sysName = urlRef.sysName;
            if (sysName) {
              title = sysName;
            }
          }
        }
      }

      let indexOfList = href.indexOf("work_order_list");
      if (indexOfList >= 0) {
        if ('faqi' == pageType) {
          title = "起草中工单"
        } else if ('paidan' == pageType) {
          title = "派单工单"
        } else if ('get' == pageType) {
          title = "待接工单"
        } else if ('chuli' == pageType) {
          title = "待处理工单"
        } else if ('pingjia' == pageType) {
          title = "待确认工单"
        } else if ('wancheng' == pageType) {
          title = "历史工单"
        } else if ('zaitu' == pageType) {
          title = "在途工单"
        } else if ('analysis' == pageType) {
          title = "统计结果工单"
        } else if ('zaitu' == pageType) {
          title = "在途工单"
        } else if ('faqi_search' == pageType) {
          title = "我报障的工单"
        } else if ('chuli_search' == pageType) {
          title = "我处理的工单"
        } else if ('pingjia_search' == pageType) {
          title = "我确认的工单"
        } else if ('paidan_search' == pageType) {
          title = "我派单的工单"
        }
      }




      if (to.fullPath && to.fullPath.indexOf("bind") == -1) {
        localStorage.setItem('toUrl', to.fullPath);
      }
      document.title = title
      const isLogin = localStorage.wsToken ? true : false;

      if (to.path === '/bind' || to.path === '/s' ||to.path === '/select_type_noauth' ||to.path === '/add_work_order_noauth'
      ||to.path === '/baozhang_success') {
        next()
      } else {
        isLogin ? next() : next({path: '/bind', query: {redirect: to.fullPath}})
      }
    }
)
export default router
