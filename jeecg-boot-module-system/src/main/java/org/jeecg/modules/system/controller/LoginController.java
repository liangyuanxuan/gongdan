package org.jeecg.modules.system.controller;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetbyunionidRequest;
import com.dingtalk.api.response.OapiUserGetbyunionidResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lst.dingtalk.Sample;
import com.lst.work.entity.SysSetting;
import com.lst.work.entity.TeamUser;
import com.lst.work.service.ISysSettingService;
import com.lst.work.service.ITeamUserService;
import io.netty.util.internal.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.*;
import org.jeecg.common.util.encryption.EncryptedString;
import org.jeecg.modules.shiro.vo.DefContants;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.model.SysLoginModel;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecg.modules.system.service.ISysDictService;
import org.jeecg.modules.system.service.ISysLogService;
import org.jeecg.modules.system.service.ISysUserService;
import org.jeecg.modules.system.util.RandImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;
import com.alibaba.fastjson.JSON;
import com.aliyun.dingtalkcontact_1_0.models.GetUserHeaders;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenRequest;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenResponse;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author scott
 * @since 2018-12-17
 */
@RestController
@RequestMapping("/sys")
@Api(tags="用户登录")
@Slf4j
public class LoginController {

	@Autowired
	private ISysUserService sysUserService;
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	@Autowired
	private ISysLogService logService;
	@Autowired
    private RedisUtil redisUtil;
	@Autowired
    private ISysDepartService sysDepartService;
	@Autowired
    private ISysDictService sysDictService;
	@Autowired
	private ITeamUserService teamUserService;
	@Autowired
	private ISysSettingService sysSettingService;
	@Value("${jeecg.errorTimes}")
	private int errorTimes;
	private static final String BASE_CHECK_CODES = "qwertyuiplkjhgfdsazxcvbnmQWERTYUPLKJHGFDSAZXCVBNM1234567890";

	@ApiOperation("登录接口")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Result<JSONObject> login(@RequestBody SysLoginModel sysLoginModel){
		Result<JSONObject> result = new Result<JSONObject>();
		String username = sysLoginModel.getUsername();

		if(!StringUtil.isNullOrEmpty(username)){
			username=username.trim();
		}
		if(!StringUtil.isNullOrEmpty(username)){
			username=username.replace(" ","");
		}

		String password = sysLoginModel.getPassword();
		if(!StringUtil.isNullOrEmpty(password)&& password.length()>=2){
			password=CommonUtils.decode(password.substring(1,password.length()-1));
		}
		//update-begin--Author:scott  Date:20190805 for：暂时注释掉密码加密逻辑，有点问题
		//前端密码加密，后端进行密码解密
		//password = AesEncryptUtil.desEncrypt(sysLoginModel.getPassword().replaceAll("%2B", "\\+")).trim();//密码解密
		//update-begin--Author:scott  Date:20190805 for：暂时注释掉密码加密逻辑，有点问题

		//update-begin-author:taoyan date:20190828 for:校验验证码
        String captcha = sysLoginModel.getCaptcha();
        if(captcha==null){
            result.error500("验证码无效");
            return result;
        }
        String lowerCaseCaptcha = captcha.toLowerCase();
		String realKey = MD5Util.MD5Encode(lowerCaseCaptcha+sysLoginModel.getCheckKey(), "utf-8");
		Object checkCode = redisUtil.get(realKey);
		if(checkCode==null || !checkCode.equals(lowerCaseCaptcha)) {
			result.error500("验证码错误");
			return result;
		}
		//update-end-author:taoyan date:20190828 for:校验验证码

		//1. 校验用户是否有效
		SysUser sysUser = sysUserService.getUserByName(username);
		if(null==sysUser){
			return result.error500("用户名或密码错误");
		}
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String todayStr=sdf.format(new Date());
		String errorDate=null==sysUser.getErrorDate()?"":sysUser.getErrorDate();
		int curErrorTime=null==sysUser.getErrorTimes()?0:sysUser.getErrorTimes();
		if(todayStr.equals(errorDate)&&curErrorTime>=errorTimes-1){
			result.error500("您今日密码输入错误次数达到上限，请明日再试或联系管理员解锁");
			return result;
		}
		result = sysUserService.checkUserIsEffective(sysUser);
		if(!result.isSuccess()) {

			return result;
		}
		
		//2. 校验用户名或密码是否正确
		String userpassword = PasswordUtil.encrypt(username, password, sysUser.getSalt());
		String syspassword = sysUser.getPassword();
		if (!syspassword.equals(userpassword)) {
			try{
				String errorMsg="";




				if(todayStr.equals(errorDate)){
					curErrorTime++;
					if(curErrorTime<errorTimes){

					}
				}else{
					errorDate=todayStr;
					curErrorTime=1;
				}
				errorMsg="您今日已输错"+curErrorTime+"次密码，剩余机会"+(errorTimes-curErrorTime)+"次";
//				sysUser.setErrorDate(todayStr);
//				sysUser.setErrorTimes(curErrorTime);
				sysUserService.update(new SysUser().setErrorDate(todayStr).setErrorTimes(curErrorTime),
						new UpdateWrapper<SysUser>().lambda().eq(SysUser::getId,sysUser.getId()));
				result.error500(errorMsg);
				return result;

			}catch (Exception e){
				log.error(e.getMessage(),e);
			}
			result.error500("用户名或密码错误");
			return result;
		}
				
		//用户登录信息
		userInfo(sysUser, result);
		sysBaseAPI.addLog("用户名: " + username + ",登录成功！", CommonConstant.LOG_TYPE_1, null);
		if("Pass_666888".equals(password)){
			result.setMessage("simplePass");
		}
		return result;
	}

	/**
	 * 退出登录
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/logout")
	public Result<Object> logout(HttpServletRequest request,HttpServletResponse response) {
		//用户退出逻辑
	    String token = request.getHeader(DefContants.X_ACCESS_TOKEN);
	    if(oConvertUtils.isEmpty(token)) {
	    	return Result.error("退出登录失败！");
	    }
	    String username = JwtUtil.getUsername(token);
		LoginUser sysUser = sysBaseAPI.getUserByName(username);
	    if(sysUser!=null) {
	    	sysBaseAPI.addLog("用户名: "+sysUser.getRealname()+",退出成功！", CommonConstant.LOG_TYPE_1, null);
	    	log.info(" 用户名:  "+sysUser.getRealname()+",退出成功！ ");
	    	//清空用户登录Token缓存
	    	redisUtil.del(CommonConstant.PREFIX_USER_TOKEN + token);
	    	//清空用户登录Shiro权限缓存
			redisUtil.del(CommonConstant.PREFIX_USER_SHIRO_CACHE + sysUser.getId());
			//清空用户的缓存信息（包括部门信息），例如sys:cache:user::<username>
			redisUtil.del(String.format("%s::%s", CacheConstant.SYS_USERS_CACHE, sysUser.getUsername()));
			//调用shiro的logout
			SecurityUtils.getSubject().logout();
	    	return Result.ok("退出登录成功！");
	    }else {
	    	return Result.error("Token无效!");
	    }
	}
	
	/**
	 * 获取访问量
	 * @return
	 */
	@GetMapping("loginfo")
	public Result<JSONObject> loginfo() {
		Result<JSONObject> result = new Result<JSONObject>();
		JSONObject obj = new JSONObject();
		//update-begin--Author:zhangweijian  Date:20190428 for：传入开始时间，结束时间参数
		// 获取一天的开始和结束时间
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date dayStart = calendar.getTime();
		calendar.add(Calendar.DATE, 1);
		Date dayEnd = calendar.getTime();
		// 获取系统访问记录
		Long totalVisitCount = logService.findTotalVisitCount();
		obj.put("totalVisitCount", totalVisitCount);
		Long todayVisitCount = logService.findTodayVisitCount(dayStart,dayEnd);
		obj.put("todayVisitCount", todayVisitCount);
		Long todayIp = logService.findTodayIp(dayStart,dayEnd);
		//update-end--Author:zhangweijian  Date:20190428 for：传入开始时间，结束时间参数
		obj.put("todayIp", todayIp);
		result.setResult(obj);
		result.success("登录成功");
		return result;
	}
	
	/**
	 * 获取访问量
	 * @return
	 */
	@GetMapping("visitInfo")
	public Result<List<Map<String,Object>>> visitInfo() {
		Result<List<Map<String,Object>>> result = new Result<List<Map<String,Object>>>();
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date dayEnd = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        Date dayStart = calendar.getTime();
        List<Map<String,Object>> list = logService.findVisitCount(dayStart, dayEnd);
		result.setResult(oConvertUtils.toLowerCasePageList(list));
		return result;
	}
	
	
	/**
	 * 登陆成功选择用户当前部门
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/selectDepart", method = RequestMethod.POST)
	public Result<JSONObject> selectDepart(@RequestBody SysUser user) {
		Result<JSONObject> result = new Result<JSONObject>();
		String username = user.getUsername();
		if(oConvertUtils.isEmpty(username)) {
			LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
			username = sysUser.getUsername();
		}
		String orgCode= user.getOrgCode();
		this.sysUserService.updateUserDepart(username, orgCode);
		SysUser sysUser = sysUserService.getUserByName(username);
		JSONObject obj = new JSONObject();
		obj.put("userInfo", sysUser);
		result.setResult(obj);
		return result;
	}

	/**
	 * 短信登录接口
	 * 
	 * @param jsonObject
	 * @return
	 */
	@PostMapping(value = "/sms")
	public Result<String> sms(@RequestBody JSONObject jsonObject) {
		Result<String> result = new Result<String>();
		String mobile = jsonObject.get("mobile").toString();
		//手机号模式 登录模式: "2"  注册模式: "1"
		String smsmode=jsonObject.get("smsmode").toString();
		log.info(mobile);
		if(oConvertUtils.isEmpty(mobile)){
			result.setMessage("手机号不允许为空！");
			result.setSuccess(false);
			return result;
		}
		Object object = redisUtil.get(mobile);
		if (object != null) {
			result.setMessage("验证码10分钟内，仍然有效！");
			result.setSuccess(false);
			return result;
		}

		//随机数
		String captcha = RandomUtil.randomNumbers(6);
		JSONObject obj = new JSONObject();
    	obj.put("code", captcha);
		try {
			boolean b = false;
			//注册模板
			if (CommonConstant.SMS_TPL_TYPE_1.equals(smsmode)) {
				SysUser sysUser = sysUserService.getUserByPhone(mobile);
				if(sysUser!=null) {
					result.error500(" 手机号已经注册，请直接登录！");
					sysBaseAPI.addLog("手机号已经注册，请直接登录！", CommonConstant.LOG_TYPE_1, null);
					return result;
				}
				b = DySmsHelper.sendSms(mobile, obj, DySmsEnum.REGISTER_TEMPLATE_CODE);
			}else {
				//登录模式，校验用户有效性
				SysUser sysUser = sysUserService.getUserByPhone(mobile);
				result = sysUserService.checkUserIsEffective(sysUser);
				if(!result.isSuccess()) {
					return result;
				}
				
				/**
				 * smsmode 短信模板方式  0 .登录模板、1.注册模板、2.忘记密码模板
				 */
				if (CommonConstant.SMS_TPL_TYPE_0.equals(smsmode)) {
					//登录模板
					b = DySmsHelper.sendSms(mobile, obj, DySmsEnum.LOGIN_TEMPLATE_CODE);
				} else if(CommonConstant.SMS_TPL_TYPE_2.equals(smsmode)) {
					//忘记密码模板
					b = DySmsHelper.sendSms(mobile, obj, DySmsEnum.FORGET_PASSWORD_TEMPLATE_CODE);
				}
			}

			if (b == false) {
				result.setMessage("短信验证码发送失败,请稍后重试");
				result.setSuccess(false);
				return result;
			}
			//验证码10分钟内有效
			redisUtil.set(mobile, captcha, 600);
			//update-begin--Author:scott  Date:20190812 for：issues#391
			//result.setResult(captcha);
			//update-end--Author:scott  Date:20190812 for：issues#391
			result.setSuccess(true);

		} catch (ClientException e) {
			e.printStackTrace();
			result.error500(" 短信接口未配置，请联系管理员！");
			return result;
		}
		return result;
	}
	

	/**
	 * 手机号登录接口
	 * 
	 * @param jsonObject
	 * @return
	 */
	@ApiOperation("手机号登录接口")
	@PostMapping("/phoneLogin")
	public Result<JSONObject> phoneLogin(@RequestBody JSONObject jsonObject) {
		Result<JSONObject> result = new Result<JSONObject>();
		String phone = jsonObject.getString("mobile");
		
		//校验用户有效性
		SysUser sysUser = sysUserService.getUserByPhone(phone);
		result = sysUserService.checkUserIsEffective(sysUser);
		if(!result.isSuccess()) {
			return result;
		}
		
		String smscode = jsonObject.getString("captcha");
		Object code = redisUtil.get(phone);
		if (!smscode.equals(code)) {
			result.setMessage("手机验证码错误");
			return result;
		}
		//用户信息
		userInfo(sysUser, result);
		//添加日志
		sysBaseAPI.addLog("用户名: " + sysUser.getUsername() + ",登录成功！", CommonConstant.LOG_TYPE_1, null);

		return result;
	}


	/**
	 * 用户信息
	 *
	 * @param sysUser
	 * @param result
	 * @return
	 */
	private Result<JSONObject> userInfo(SysUser sysUser, Result<JSONObject> result) {
		String syspassword = sysUser.getPassword();
		String username = sysUser.getUsername();
		// 生成token
		String token = JwtUtil.sign(username, syspassword);
        // 设置token缓存有效时间
		redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
		redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME*2 / 1000);

		// 获取用户部门信息
		JSONObject obj = new JSONObject();
		List<SysDepart> departs = sysDepartService.queryUserDeparts(sysUser.getId());
		obj.put("departs", departs);
		if (departs == null || departs.size() == 0) {
			obj.put("multi_depart", 0);
		} else if (departs.size() == 1) {
			sysUserService.updateUserDepart(username, departs.get(0).getOrgCode());
			obj.put("multi_depart", 1);
		} else {
			obj.put("multi_depart", 2);
		}
		List<String> roles=sysBaseAPI.getRolesByUsername(sysUser.getUsername());
		String roleMark="";
		if(null!=roles&&roles.size()>0) {
			for (String r : roles) {
				roleMark += r + ",";
			}
		}
		sysUser.setEmail(roleMark);
		obj.put("token", token);
		obj.put("userInfo", sysUser);
		obj.put("sysAllDictItems", sysDictService.queryAllDictItems());
		result.setResult(obj);
		result.success("登录成功");
		return result;
	}
	/**
	 * 钉钉登录接口
	 *
	 * @return
	 */
	@ApiOperation("钉钉登录接口")
	@GetMapping(value="/getUserInfoByDingId")
	public Result getUserInfoByDingId(@ApiParam(name = "钉钉Id", value = "dingId", required = true) @RequestParam(name="dingId",required=true)  String dingId) {
		if(StringUtil.isNullOrEmpty(dingId)){
			return Result.error("dingId不能为空");
		}
		//1. 校验用户是否有效
		SysUser sysUser = sysUserService.getUserByName(dingId);
		if(null==sysUser){
			return Result.error("not exists");
		}
		String syspassword = sysUser.getPassword();
		String username = sysUser.getUsername();
		// 生成token
		String token = JwtUtil.sign(username, syspassword);
		// 设置token缓存有效时间
		redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
		redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME*2 / 1000);
		QueryWrapper<TeamUser> teamUserQueryWrapper=new QueryWrapper<TeamUser>();
		teamUserQueryWrapper.eq("user",sysUser.getId());
		List<TeamUser> teamUsers=teamUserService.list(teamUserQueryWrapper);
		// 获取用户部门信息
		JSONObject obj = new JSONObject();
		List<SysDepart> departs = sysDepartService.queryUserDeparts(sysUser.getId());

		obj.put("departs", departs);
		String orgName="";
		sysUser.setOrgCode("");
			QueryWrapper<SysDepart> deptQueryWrapper=new QueryWrapper<SysDepart>();
			deptQueryWrapper.eq("org_type","1");
			List<SysDepart> departList=sysDepartService.list(deptQueryWrapper);
			if(null!=departList&&departList.size()>0) {
//				sysUser.setOrgCode(orgName)
				orgName=departList.get(0).getDepartName();
				sysUser.setOrgCode(orgName);
			}

		if (departs == null || departs.size() == 0) {
			obj.put("multi_depart", 0);
		} else if (departs.size() == 1) {
			sysUserService.updateUserDepart(username, departs.get(0).getOrgCode());
			obj.put("multi_depart", 1);
		} else {
			obj.put("multi_depart", 2);
		}
		List<String> roles=sysBaseAPI.getRolesByUsername(sysUser.getUsername());
		String roleMark="";
		if(null!=roles&&roles.size()>0) {
			for (String r : roles) {
				roleMark += r + ",";
			}
		}
		sysUser.setEmail(roleMark);
		obj.put("token", token);
		obj.put("userInfo", sysUser);
		obj.put("teamUsers", teamUsers);
		obj.put("sysAllDictItems", sysDictService.queryAllDictItems());
		return Result.ok(obj);
	}
	/**
	 * 钉钉绑定接口
	 *
	 * @return
	 */
	@ApiOperation("钉钉绑定接口")
	@GetMapping("/bindDing")
	public Result bindDing(@ApiParam(name = "钉钉Id", value = "dingId", required = true) @RequestParam(name="dingId",required=true)  String dingId
	,@ApiParam(name = "用户名", value = "userName", required = true) @RequestParam(name="userName",required=true)  String userName) {


		//1. 校验用户是否有效
		SysUser sysUser = sysUserService.getUserByName(userName);
		if(null==sysUser){
			return Result.error("用户不存在");
		}
		sysUserService.updateUserDingId(userName,dingId);

		return Result.ok("绑定成功");
	}
	/**
	 * 钉钉解绑接口
	 *
	 * @return
	 */
	@ApiOperation("钉钉解绑接口")
	@GetMapping("/unBindDing")
	public Result unBindDing(@ApiParam(name = "用户名", value = "userName", required = true) @RequestParam(name="userName",required=true)  String userName) {

		//1. 校验用户是否有效
		SysUser sysUser = sysUserService.getUserByName(userName);
		if(null==sysUser){
			return Result.error("用户不存在");
		}
		sysUserService.updateUserDingId(userName,null);

		return Result.ok("解绑成功");
	}
	/**
	 * 获取加密字符串
	 * @return
	 */
	@GetMapping(value = "/getEncryptedString")
	public Result<Map<String,String>> getEncryptedString(){
		Result<Map<String,String>> result = new Result<Map<String,String>>();
		Map<String,String> map = new HashMap<String,String>();
		map.put("key", EncryptedString.key);
		map.put("iv",EncryptedString.iv);
		result.setResult(map);
		return result;
	}

	/**
	 * 后台生成图形验证码 ：有效
	 * @param response
	 * @param key
	 */
	@ApiOperation("获取验证码")
	@GetMapping(value = "/randomImage/{key}")
	public Result<String> randomImage(HttpServletResponse response,@PathVariable String key){
		Result<String> res = new Result<String>();
		try {
			String code = RandomUtil.randomString(BASE_CHECK_CODES,4);
			String lowerCaseCode = code.toLowerCase();
			String realKey = MD5Util.MD5Encode(lowerCaseCode+key, "utf-8");
			redisUtil.set(realKey, lowerCaseCode, 60*3);
			String base64 = RandImageUtil.generate(code);
			res.setSuccess(true);
			res.setResult(base64);
		} catch (Exception e) {
			res.error500("获取验证码出错"+e.getMessage());
			e.printStackTrace();
		}
		return res;
	}
	
	/**
	 * app登录
	 * @param sysLoginModel
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mLogin", method = RequestMethod.POST)
	public Result<JSONObject> mLogin(@RequestBody SysLoginModel sysLoginModel) throws Exception {
		Result<JSONObject> result = new Result<JSONObject>();
		String username = sysLoginModel.getUsername();
		String password = sysLoginModel.getPassword();
		
		//1. 校验用户是否有效
		SysUser sysUser = sysUserService.getUserByName(username);
		result = sysUserService.checkUserIsEffective(sysUser);
		if(!result.isSuccess()) {
			return result;
		}
		
		//2. 校验用户名或密码是否正确
		String userpassword = PasswordUtil.encrypt(username, password, sysUser.getSalt());
		String syspassword = sysUser.getPassword();
		if (!syspassword.equals(userpassword)) {
			result.error500("用户名或密码错误");
			return result;
		}
		
		String orgCode = sysUser.getOrgCode();
		if(oConvertUtils.isEmpty(orgCode)) {
			//如果当前用户无选择部门 查看部门关联信息
			List<SysDepart> departs = sysDepartService.queryUserDeparts(sysUser.getId());
			if (departs == null || departs.size() == 0) {
				result.error500("用户暂未归属部门,不可登录!");
				return result;
			}
			orgCode = departs.get(0).getOrgCode();
			sysUser.setOrgCode(orgCode);
			this.sysUserService.updateUserDepart(username, orgCode);
		}
		JSONObject obj = new JSONObject();
		//用户登录信息
		obj.put("userInfo", sysUser);
		
		// 生成token
		String token = JwtUtil.sign(username, syspassword);
		// 设置超时时间
		redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
		redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME*2 / 1000);
		//token 信息
		obj.put("token", token);
		result.setResult(obj);
		result.setSuccess(true);
		result.setCode(200);
		sysBaseAPI.addLog("用户名: " + username + ",登录成功[移动端]！", CommonConstant.LOG_TYPE_1, null);
		return result;
	}
	/**
	 * app登录
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/jkLogin", method = RequestMethod.POST)
	public String jkLogin( HttpServletRequest request) throws Exception {
		Result<JSONObject> result = new Result<JSONObject>();
//		String username = sysLoginModel.getUsername();
//		String password = sysLoginModel.getPassword();
		String username=request.getParameter("username");
		String password=request.getParameter("password");

		//1. 校验用户是否有效
		SysUser sysUser = sysUserService.getUserByName(username);
		result = sysUserService.checkUserIsEffective(sysUser);
		if(!result.isSuccess()) {
			return new ObjectMapper().writeValueAsString(result);
		}

		//2. 校验用户名或密码是否正确
		String userpassword = PasswordUtil.encrypt(username, password, sysUser.getSalt());
		String syspassword = sysUser.getPassword();
		if (!syspassword.equals(userpassword)) {
			result.error500("用户名或密码错误");
			return new ObjectMapper().writeValueAsString(result);
		}

		String orgCode = sysUser.getOrgCode();
//		if(oConvertUtils.isEmpty(orgCode)) {
//			//如果当前用户无选择部门 查看部门关联信息
//			List<SysDepart> departs = sysDepartService.queryUserDeparts(sysUser.getId());
//			if (departs == null || departs.size() == 0) {
//				result.error500("用户暂未归属部门,不可登录!");
//				return result;
//			}
//			orgCode = departs.get(0).getOrgCode();
//			sysUser.setOrgCode(orgCode);
//			this.sysUserService.updateUserDepart(username, orgCode);
//		}
		JSONObject obj = new JSONObject();
		//用户登录信息
		obj.put("userInfo", sysUser);

		// 生成token
		String token = JwtUtil.sign(username, syspassword);
		// 设置超时时间
		redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
		redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME*2 / 1000);
		//token 信息
//		obj.put("token", token);
//		result.setResult(obj);
//		result.setSuccess(true);
//		result.setCode(200);
		sysBaseAPI.addLog("用户名: " + username + ",登录成功[三方接口]！", CommonConstant.LOG_TYPE_1, null);
		return token;
	}

	/**
	 * 图形验证码
	 * @param sysLoginModel
	 * @return
	 */
	@RequestMapping(value = "/checkCaptcha", method = RequestMethod.POST)
	public Result<?> checkCaptcha(@RequestBody SysLoginModel sysLoginModel){
		String captcha = sysLoginModel.getCaptcha();
		String checkKey = sysLoginModel.getCheckKey();
		if(captcha==null){
			return Result.error("验证码无效");
		}
		String lowerCaseCaptcha = captcha.toLowerCase();
		String realKey = MD5Util.MD5Encode(lowerCaseCaptcha+checkKey, "utf-8");
		Object checkCode = redisUtil.get(realKey);
		if(checkCode==null || !checkCode.equals(lowerCaseCaptcha)) {
			return Result.error("验证码错误");
		}
		return Result.ok();
	}

	public static com.aliyun.dingtalkoauth2_1_0.Client authClient() throws Exception {
		Config config = new Config();
		config.protocol = "https";
		config.regionId = "central";
		return new com.aliyun.dingtalkoauth2_1_0.Client(config);
	}
	/**
	 * 获取用户token
	 * @param authCode
	 * @return
	 * @throws Exception
	 */
	//接口地址：注意/auth与钉钉登录与分享的回调域名地址一致
	@RequestMapping(value = "/auth", method = RequestMethod.GET)
	public Result getAccessToken(@RequestParam(value = "authCode")String authCode)  {


		String domain="";
		String agentId="";
		String appKey="";
		String appSecret="";
		String corpId="";
		List<SysSetting> sysSettings=sysSettingService.list();
		if(null!=sysSettings&&sysSettings.size()>0) {
			for (SysSetting sysSetting : sysSettings) {
				if ("1".equals(sysSetting.getIsValid())) {
					if ("domain".equals(sysSetting.getCode())) {
						domain = sysSetting.getValue();
					}
					if ("agentId".equals(sysSetting.getCode())) {
						agentId = sysSetting.getValue();
					}
					if ("appKey".equals(sysSetting.getCode())) {
						appKey = sysSetting.getValue();
					}
					if ("appSecret".equals(sysSetting.getCode())) {
						appSecret = sysSetting.getValue();
					}
					if ("corpId".equals(sysSetting.getCode())) {
						corpId = sysSetting.getValue();
					}
				}
			}
		}

		JSONObject obj = new JSONObject();
		try{
		com.aliyun.dingtalkoauth2_1_0.Client client = authClient();
		GetUserTokenRequest getUserTokenRequest = new GetUserTokenRequest()

				//应用基础信息-应用信息的AppKey,请务必替换为开发的应用AppKey
				.setClientId(appKey)

				//应用基础信息-应用信息的AppSecret，,请务必替换为开发的应用AppSecret
				.setClientSecret(appSecret)
				.setCode(authCode)
				.setGrantType("authorization_code");
		GetUserTokenResponse getUserTokenResponse = null;

			getUserTokenResponse =client.getUserToken(getUserTokenRequest);
			//获取用户个人token
			String userAccessToken = getUserTokenResponse.getBody().getAccessToken();

			String accessToken = Sample.getAccessToken(appKey, appSecret);
			String dingId=getUserinfo(userAccessToken,accessToken);

			//1. 校验用户是否有效
			SysUser sysUser = sysUserService.getUserByName(dingId);
			if(null==sysUser){
				return Result.error("not exists");
			}
			String syspassword = sysUser.getPassword();
			String username = sysUser.getUsername();
			// 生成token
			String token = JwtUtil.sign(username, syspassword);
			// 设置token缓存有效时间
			redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
			redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME*2 / 1000);
			QueryWrapper<TeamUser> teamUserQueryWrapper=new QueryWrapper<TeamUser>();
			teamUserQueryWrapper.eq("user",sysUser.getId());
			List<TeamUser> teamUsers=teamUserService.list(teamUserQueryWrapper);
			// 获取用户部门信息

			List<SysDepart> departs = sysDepartService.queryUserDeparts(sysUser.getId());

			obj.put("departs", departs);
			String orgName="";
			sysUser.setOrgCode("");
			QueryWrapper<SysDepart> deptQueryWrapper=new QueryWrapper<SysDepart>();
			deptQueryWrapper.eq("org_type","1");
			List<SysDepart> departList=sysDepartService.list(deptQueryWrapper);
			if(null!=departList&&departList.size()>0) {
//				sysUser.setOrgCode(orgName)
				orgName=departList.get(0).getDepartName();
				sysUser.setOrgCode(orgName);
			}

			if (departs == null || departs.size() == 0) {
				obj.put("multi_depart", 0);
			} else if (departs.size() == 1) {
				sysUserService.updateUserDepart(username, departs.get(0).getOrgCode());
				obj.put("multi_depart", 1);
			} else {
				obj.put("multi_depart", 2);
			}
			List<String> roles=sysBaseAPI.getRolesByUsername(sysUser.getUsername());
			String roleMark="";
			if(null!=roles&&roles.size()>0) {
				for (String r : roles) {
					roleMark += r + ",";
				}
			}
			sysUser.setEmail(roleMark);
			obj.put("token", token);
			obj.put("userInfo", sysUser);
			obj.put("teamUsers", teamUsers);
			obj.put("sysAllDictItems", sysDictService.queryAllDictItems());


		}catch (Exception e){
			log.error(e.getMessage(),e);

		}
		return Result.ok(obj);
	}
	public static com.aliyun.dingtalkcontact_1_0.Client contactClient() throws Exception {
		Config config = new Config();
		config.protocol = "https";
		config.regionId = "central";
		return new com.aliyun.dingtalkcontact_1_0.Client(config);
	}
	/**
	 * 获取用户个人信息
	 * @param accessToken
	 * @return
	 * @throws Exception
	 */
	public String getUserinfo(String userAccessToken,String accessToken) throws Exception {
		com.aliyun.dingtalkcontact_1_0.Client client = contactClient();
		GetUserHeaders getUserHeaders = new GetUserHeaders();
		getUserHeaders.xAcsDingtalkAccessToken = userAccessToken;
		try {
			//获取用户个人信息
			String me = JSON.toJSONString(client.getUserWithOptions("me", getUserHeaders, new RuntimeOptions()).getBody());
			log.info(me);
			String unionId = client.getUserWithOptions("me", getUserHeaders, new RuntimeOptions()).getBody().getUnionId();
			DingTalkClient clientUnionId = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/user/getbyunionid");
			OapiUserGetbyunionidRequest req = new OapiUserGetbyunionidRequest();
			req.setUnionid(unionId);
			OapiUserGetbyunionidResponse rsp = clientUnionId.execute(req, accessToken);
			log.info(rsp.getBody());
			String dingId = rsp.getResult().getUserid();
			return dingId;
//			if(!StringUtil.isNullOrEmpty(dingId)){
//
//			}
		}catch (Exception e ){
			log.error(e.getMessage(),e);
		}
//		System.out.println(rsp.getBody());

//		System.out.println(me);
//		log.info(me);
		return null;
	}


}