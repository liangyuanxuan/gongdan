package com.lst.work.entity;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Description: 工单类型
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Data
@TableName("work_order_type")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="work_order_type对象", description="工单类型")
public class WorkOrderTypeView implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**类别编号*/
	@Excel(name = "类别编号", width = 15)
    @ApiModelProperty(value = "类别编号")
    private String code;
	/**类别名称*/
	@Excel(name = "类别名称", width = 15)
    @ApiModelProperty(value = "类别名称")
    private String name;
    /**派单超时小时数*/
    @Excel(name = "派单超时小时数", width = 15)
    @ApiModelProperty(value = "派单超时小时数")
    @TableField(strategy = FieldStrategy.IGNORED)
    private BigDecimal overtimeHoursPaidan;
    /**接单超时小时数*/
    @Excel(name = "接单超时小时数", width = 15)
    @ApiModelProperty(value = "接单超时小时数")
    @TableField(strategy = FieldStrategy.IGNORED)
    private BigDecimal overtimeHoursGet;
    /**处理超时小时数*/
    @Excel(name = "处理超时小时数", width = 15)
    @ApiModelProperty(value = "处理超时小时数")
    @TableField(strategy = FieldStrategy.IGNORED)
    private BigDecimal overtimeHoursHandle;
    /**评价超时小时数*/
    @Excel(name = "评价超时小时数", width = 15)
    @ApiModelProperty(value = "评价超时小时数")
    @TableField(strategy = FieldStrategy.IGNORED)
    private BigDecimal overtimeHoursScore;
    /**默认工程师*/
    @Excel(name = "默认工程师", width = 15,dictTable = "sys_user", dicText = "realname", dicCode = "id")
    @ApiModelProperty(value = "默认工程师")
    @Dict(dictTable = "sys_user", dicText = "realname", dicCode = "id")
    private java.lang.String defaultToUser;
    /**重复订单默认处理工程师*/
    @Excel(name = "重复订单默认处理工程师", width = 15,dictTable = "sys_user", dicText = "realname", dicCode = "id")
    @Dict(dictTable = "sys_user", dicText = "realname", dicCode = "id")
    @ApiModelProperty(value = "重复订单默认处理工程师")
    private java.lang.String defaultDuplyToUser;
    /**派单人*/
    @ApiModelProperty(value = "派单人")
    private List<String> alignUser;
    /**处理团队*/
    @ApiModelProperty(value = "处理团队")
    private List<String> handleTeam;
    /**派单人*/
    @ApiModelProperty(value = "派单人")
    private List<JSONObject> alignUserObj;
    /**处理团队*/
    @ApiModelProperty(value = "处理团队")
    private List<JSONObject> handleTeamObj;
	/**排序*/
	@Excel(name = "排序", width = 15)
    @ApiModelProperty(value = "排序")
    private String orderNum;
    /**重复提醒消息抄送派单者*/
    @Excel(name = "重复提醒消息抄送派单者", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    @ApiModelProperty(value = "重复提醒消息抄送派单者")
    private java.lang.String duplyMsgToAlignUser;
    /**是否自动接单*/
    @Excel(name = "是否自动接单", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否自动接单")
    private java.lang.String autoGet;

    /**是否超时自动确认*/
    @Excel(name = "是否超时自动确认", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否超时自动确认")
    private java.lang.String autoQueren;
    /**处理时长从派单计算*/
    @Excel(name = "处理时长从派单计算", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    @ApiModelProperty(value = "处理时长从派单计算")
    private java.lang.String solveTimeFromAlign;
    /**超时自动确认小时数*/
    @Excel(name = "超时自动确认小时数", width = 15)
    @ApiModelProperty(value = "超时自动确认小时数")
    @TableField(strategy = FieldStrategy.IGNORED)
    private BigDecimal autoQuerenHours;
}
