package com.lst.work.service.impl;

import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.DictArea;
import com.lst.work.mapper.DictAreaMapper;
import com.lst.work.service.IDictAreaService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 地区
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
@Service
public class DictAreaServiceImpl extends ServiceImpl<DictAreaMapper, DictArea> implements IDictAreaService {

	@Override
	public void addDictArea(DictArea dictArea) {
		if(oConvertUtils.isEmpty(dictArea.getPid())){
			dictArea.setPid(IDictAreaService.ROOT_PID_VALUE);
		}else{
			//如果当前节点父ID不为空 则设置父节点的hasChildren 为1
			DictArea parent = baseMapper.selectById(dictArea.getPid());
			if(parent!=null && !"1".equals(parent.getHasChild())){
				parent.setHasChild("1");
				baseMapper.updateById(parent);
			}
		}
		baseMapper.insert(dictArea);
	}
	
	@Override
	public void updateDictArea(DictArea dictArea) {
		DictArea entity = this.getById(dictArea.getId());
		if(entity==null) {
			throw new JeecgBootException("未找到对应实体");
		}
		String old_pid = entity.getPid();
		String new_pid = dictArea.getPid();
		if(!old_pid.equals(new_pid)) {
			updateOldParentNode(old_pid);
			if(oConvertUtils.isEmpty(new_pid)){
				dictArea.setPid(IDictAreaService.ROOT_PID_VALUE);
			}
			if(!IDictAreaService.ROOT_PID_VALUE.equals(dictArea.getPid())) {
				baseMapper.updateTreeNodeStatus(dictArea.getPid(), IDictAreaService.HASCHILD);
			}
		}
		baseMapper.updateById(dictArea);
	}
	
	@Override
	public void deleteDictArea(String id) throws JeecgBootException {
		DictArea dictArea = this.getById(id);
		if(dictArea==null) {
			throw new JeecgBootException("未找到对应实体");
		}
		updateOldParentNode(dictArea.getPid());
		baseMapper.deleteById(id);
	}
	
	
	
	/**
	 * 根据所传pid查询旧的父级节点的子节点并修改相应状态值
	 * @param pid
	 */
	private void updateOldParentNode(String pid) {
		if(!IDictAreaService.ROOT_PID_VALUE.equals(pid)) {
			Integer count = baseMapper.selectCount(new QueryWrapper<DictArea>().eq("pid", pid));
			if(count==null || count<=1) {
				baseMapper.updateTreeNodeStatus(pid, IDictAreaService.NOCHILD);
			}
		}
	}

}
