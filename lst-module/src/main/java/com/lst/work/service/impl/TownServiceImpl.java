package com.lst.work.service.impl;

import com.lst.work.entity.Town;
import com.lst.work.mapper.TownMapper;
import com.lst.work.service.ITownService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 镇
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
@Service
public class TownServiceImpl extends ServiceImpl<TownMapper, Town> implements ITownService {

}
