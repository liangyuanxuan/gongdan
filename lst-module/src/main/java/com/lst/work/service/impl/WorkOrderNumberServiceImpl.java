package com.lst.work.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lst.work.entity.WorkOrderNumber;
import com.lst.work.entity.WorkOrderType;
import com.lst.work.mapper.WorkOrderNumberMapper;
import com.lst.work.service.IWorkOrderNumberService;
import com.lst.work.service.IWorkOrderTypeService;
import io.netty.util.internal.StringUtil;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 工单编号
 * @Author: jeecg-boot
 * @Date:   2022-08-13
 * @Version: V1.0
 */
@Service
public class WorkOrderNumberServiceImpl extends ServiceImpl<WorkOrderNumberMapper, WorkOrderNumber> implements IWorkOrderNumberService {

    @Autowired
    private IWorkOrderTypeService workOrderTypeService;

    public String getMax(String workOrderType)throws Exception{
        if(!StringUtil.isNullOrEmpty(workOrderType)){
            WorkOrderType workOrderTypeObj=workOrderTypeService.getById(workOrderType);
            QueryWrapper<WorkOrderNumber> queryWrapper=new QueryWrapper<WorkOrderNumber>();
            queryWrapper.eq("work_order_type",workOrderType);
            List<WorkOrderNumber> workOrderNumbers=this.list(queryWrapper);
            if(null!=workOrderNumbers&&workOrderNumbers.size()>0){
                int maxNumber=workOrderNumbers.get(0).getMaxNumber();
                maxNumber++;
                workOrderNumbers.get(0).setMaxNumber(maxNumber);
                this.updateById(workOrderNumbers.get(0));
                String numVal=String.valueOf(maxNumber);
                int len=numVal.length();
                if(len<6){
                    for(int i=0;i<6-len;i++){
                        numVal="0"+numVal;
                    }
                }else{
                    throw new Exception("序列号到上限");
                }
                if(null!=workOrderTypeObj){
                    if(!StringUtil.isNullOrEmpty(workOrderTypeObj.getCode())){
                        numVal=workOrderTypeObj.getCode()+numVal;
                    }
                }else{
                    throw new Exception("work_order_type不存在");
                }
                return numVal;
            }else{
                throw new Exception("数据异常");
            }

        }else{
            throw new Exception("workOrderType不能为空");
        }
    }
}
