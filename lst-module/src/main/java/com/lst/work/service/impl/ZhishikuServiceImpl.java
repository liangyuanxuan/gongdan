package com.lst.work.service.impl;

import com.lst.work.entity.Zhishiku;
import com.lst.work.mapper.ZhishikuMapper;
import com.lst.work.service.IZhishikuService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 知识库
 * @Author: jeecg-boot
 * @Date:   2022-10-16
 * @Version: V1.0
 */
@Service
public class ZhishikuServiceImpl extends ServiceImpl<ZhishikuMapper, Zhishiku> implements IZhishikuService {

}
