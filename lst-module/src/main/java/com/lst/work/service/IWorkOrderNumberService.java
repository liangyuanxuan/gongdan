package com.lst.work.service;

import com.lst.work.entity.WorkOrderNumber;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 工单编号
 * @Author: jeecg-boot
 * @Date:   2022-08-13
 * @Version: V1.0
 */
public interface IWorkOrderNumberService extends IService<WorkOrderNumber> {
    public String getMax(String workOrderType)throws Exception;
}
