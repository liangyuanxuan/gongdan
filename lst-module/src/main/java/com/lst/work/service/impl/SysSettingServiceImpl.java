package com.lst.work.service.impl;

import com.lst.work.entity.SysSetting;
import com.lst.work.mapper.SysSettingMapper;
import com.lst.work.service.ISysSettingService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 系统设置
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
@Service
public class SysSettingServiceImpl extends ServiceImpl<SysSettingMapper, SysSetting> implements ISysSettingService {

}
