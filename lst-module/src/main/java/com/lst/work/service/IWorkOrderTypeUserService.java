package com.lst.work.service;

import com.lst.work.entity.WorkOrderTypeUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 工单类型派单用户
 * @Author: jeecg-boot
 * @Date:   2022-08-12
 * @Version: V1.0
 */
public interface IWorkOrderTypeUserService extends IService<WorkOrderTypeUser> {
    public void editPlus(String typeId,String userId);
}
