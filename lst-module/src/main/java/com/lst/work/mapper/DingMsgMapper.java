package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.DingMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 钉钉消息
 * @Author: jeecg-boot
 * @Date:   2022-08-22
 * @Version: V1.0
 */
public interface DingMsgMapper extends BaseMapper<DingMsg> {

}
