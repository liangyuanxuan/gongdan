package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.WorkOrderType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 工单类型
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
public interface WorkOrderTypeMapper extends BaseMapper<WorkOrderType> {

}
