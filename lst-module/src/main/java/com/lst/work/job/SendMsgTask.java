package com.lst.work.job;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lst.work.entity.*;
import com.lst.work.service.*;
import com.lst.work.util.SendMsgThread;
import com.lst.work.util.WorkConst;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 
 * @ClassName:SmsSendTask 
 * @Description: 自动生成JSon定时任务
 * @date 2014-11-13 下午5:06:34
 * 
 */
@Component
@Slf4j
public class SendMsgTask implements ApplicationRunner {
	private static final Logger logger=LoggerFactory.getLogger(com.lst.work.job.SendMsgTask.class);

	Long sleepTime=1*60*1000L;
	@Value("${spring.quartz.white_list}")
	private String classList;
	@Autowired
	private IWorkOrderTypeService workOrderTypeService;
	@Autowired
	private IDingMsgService dingMsgService;
	@Autowired
	private IDeadlineMsgService deadlineMsgService;
	@Autowired
	private IWorkOrderService workOrderService;
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	@Autowired
	private IWorkOrderTypeUserService workOrderTypeUserService;
	@Autowired
	private IWorkOrderTypeDiyColumnService workOrderTypeDiyColumnService;
	@Autowired
	private ISysSettingService sysSettingService;
	@Autowired
	private IWorkOrderRealignService workOrderRealignService;

	public void run(ApplicationArguments var1) throws Exception {

		String curClass=this.getClass().getCanonicalName();
		if(!StringUtil.isNullOrEmpty(classList)&&classList.indexOf(curClass)>=0){

		}else{
			logger.info("===================任务不在运行列表中，不予开始===================");
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				logger.error(e.getMessage(),e);
			}
			return;

		}


		logger.info("===================发送消息/自动归档定时任务开始===================");
		new StartThread().start();
	}
		public class StartThread extends  Thread{
			@Override
			public void run(){
				String domain="";
				String agentId="";
				String appKey="";
				String appSecret="";

				List<SysSetting> sysSettings=sysSettingService.list();
				if(null!=sysSettings&&sysSettings.size()>0){
					for(SysSetting sysSetting:sysSettings){
						if("1".equals(sysSetting.getIsValid())){
							if("domain".equals(sysSetting.getCode())){
								domain=sysSetting.getValue();
							}
							if("agentId".equals(sysSetting.getCode())){
								agentId=sysSetting.getValue();
							}
							if("appKey".equals(sysSetting.getCode())){
								appKey=sysSetting.getValue();
							}
							if("appSecret".equals(sysSetting.getCode())){
								appSecret=sysSetting.getValue();
							}
						}
					}
				}else{
					log.error("数据异常");
				}
				while(true) {
					try{
						List<WorkOrderType> workOrderTypes=workOrderTypeService.list();

						QueryWrapper<DeadlineMsg> deadlineMsgQueryWrapperAll=new QueryWrapper<>();
						deadlineMsgQueryWrapperAll.eq("is_valid",1);
						List<DeadlineMsg> deadlineMsgsAll=deadlineMsgService.list(deadlineMsgQueryWrapperAll);

						List<String> deadTypes=new ArrayList<>();
						List<String> deadStatus=new ArrayList<>();
						deadTypes.add("empty");
						deadStatus.add("empty");
						if(null!=deadlineMsgsAll&&deadlineMsgsAll.size()>0){
							for(DeadlineMsg deadlineMsg:deadlineMsgsAll){
								deadTypes.add(deadlineMsg.getWorkOrderType());
								deadStatus.add(deadlineMsg.getStatus());
							}
						}
						deadStatus.add(WorkConst.ORDER_STATUS_SCORE);
						deadStatus.add(WorkConst.ORDER_STATUS_GET);
						QueryWrapper<WorkOrder> workOrderQueryWrapper=new QueryWrapper<>();
						workOrderQueryWrapper.in("status", deadStatus);
						workOrderQueryWrapper.in("type", deadTypes);
						List<WorkOrder> workOrders=workOrderService.list(workOrderQueryWrapper);
						if(null!=workOrders){
							for(WorkOrder workOrder:workOrders){
								log.info("开始处理工单"+workOrder.getNumber());
								try{
									if(null!=deadlineMsgsAll&&deadlineMsgsAll.size()>0){
										List<DeadlineMsg> deadlineMsgs=deadlineMsgsAll.stream().filter(item->
												item.getStatus().equals(workOrder.getStatus())&&item.getWorkOrderType().equals(workOrder.getType())
										).collect(Collectors.toList());
										if(null!=deadlineMsgs&&deadlineMsgs.size()>0){
											DeadlineMsg targetDeadlineMsg=deadlineMsgs.get(0);
											String type=workOrder.getType();
											Optional<WorkOrderType> orderOptional= workOrderTypes.stream().filter(item->item.getId().equals(type)).findFirst();
											if(null!=orderOptional&&orderOptional.isPresent()){

												WorkOrderType workOrderType=orderOptional.get();
												Date statusTime=null;
												BigDecimal daoqiHourHandle=new BigDecimal(0); //处理小时数 适用于处理时长从派单计算场景
												Date warnTime=null;
												BigDecimal beginHour=targetDeadlineMsg.getBeginHour();
												BigDecimal intervalHour=targetDeadlineMsg.getIntervalHour();
												Integer maxAlarmTimes=targetDeadlineMsg.getMaxAlarmTimes();
												if(null!=workOrderType){
													SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
													BigDecimal daoqiHour=new BigDecimal(0);
													if(WorkConst.ORDER_STATUS_ALIGN.equals(targetDeadlineMsg.getStatus())){
														statusTime=workOrder.getCreateTime();
														daoqiHour=workOrderType.getOvertimeHoursPaidan();

														log.info("有类型和状态都匹配的配置：服务项目——"+workOrderType.getName()+
																"|状态——派单|状态时间——"+sdf.format(statusTime)+
																"|环节到期时间——"+daoqiHour.doubleValue());
													}else if(WorkConst.ORDER_STATUS_GET.equals(targetDeadlineMsg.getStatus())){
														statusTime=workOrder.getAlignTime();
														daoqiHour=workOrderType.getOvertimeHoursGet();
														daoqiHourHandle=workOrderType.getOvertimeHoursHandle(); //处理小时数 适用于处理时长从派单计算场景
														log.info("有类型和状态都匹配的配置：服务项目——"+workOrderType.getName()+
																"|状态——接单|状态时间——"+sdf.format(statusTime)+
																"|环节到期时间——"+daoqiHour.doubleValue());
													}else if(WorkConst.ORDER_STATUS_HANDLE.equals(targetDeadlineMsg.getStatus())){
														if("1".equals(workOrderType.getSolveTimeFromAlign())){
															//处理环节的开始时间是派单时间
															statusTime=workOrder.getAlignTime();
														}else{
															statusTime=workOrder.getGetTime();
														}

														daoqiHour=workOrderType.getOvertimeHoursHandle();
														log.info("有类型和状态都匹配的配置：服务项目——"+workOrderType.getName()+
																"|状态——处理|状态时间——"+sdf.format(statusTime)+
																"|环节到期时间——"+daoqiHour.doubleValue());
													}else if(WorkConst.ORDER_STATUS_SCORE.equals(targetDeadlineMsg.getStatus())){
														statusTime=workOrder.getSolveTime();
														daoqiHour=workOrderType.getOvertimeHoursScore();
														log.info("有类型和状态都匹配的配置：服务项目——"+workOrderType.getName()+
																"|状态——评价|状态时间——"+sdf.format(statusTime)+
																"|环节到期时间——"+daoqiHour.doubleValue());
													}else{
														log.info("有类型和状态都匹配的配置：服务项目——"+workOrderType.getName()+"|状态——"+workOrder.getStatus());
													}
													Long chaoshiHours=new Long(0);
													if(null!=beginHour&&null!=statusTime&&null!=maxAlarmTimes){

														BigDecimal integerPartEnd = daoqiHour.setScale(0, RoundingMode.DOWN);
														BigDecimal fractionalPartEnd = daoqiHour.subtract(integerPartEnd);
														int hourValueEnd=integerPartEnd.intValue();
														int minuteValueEnd=fractionalPartEnd.multiply(new BigDecimal("60")).intValue();
														log.info("环节到期小时分钟数："+hourValueEnd+"——"+minuteValueEnd);
														Calendar calendarEnd=Calendar.getInstance();
														calendarEnd.setTime(statusTime);

														if(0!=hourValueEnd){
															calendarEnd.add(Calendar.HOUR_OF_DAY,hourValueEnd);
														}
														if(0!=minuteValueEnd){
															calendarEnd.add(Calendar.MINUTE,minuteValueEnd);
														}
														Date daoqiTime=calendarEnd.getTime();//到期时间
														log.info("计算后的环节到期时间："+sdf.format(daoqiTime));

														//
														BigDecimal integerPart = beginHour.setScale(0, RoundingMode.DOWN);
														BigDecimal fractionalPart = beginHour.subtract(integerPart);
														int hourValue=integerPart.intValue();
														int minuteValue=fractionalPart.multiply(new BigDecimal("60")).intValue();
														Calendar calendar=Calendar.getInstance();
														calendar.setTime(daoqiTime);
														log.info("提前通知小时分钟数："+hourValue+"——"+minuteValue);
														int warnTimes=0;//提醒次数
														//重复一次
														if(0!=hourValue){
															calendar.add(Calendar.HOUR_OF_DAY,0-hourValue);
														}
														if(0!=minuteValue){
															calendar.add(Calendar.MINUTE,0-minuteValue);
														}
														warnTime=calendar.getTime();//提醒时间
														log.info("计算后的开始通知时间："+sdf.format(warnTime));

														try{
															chaoshiHours=dateDiffHour(daoqiTime,new Date());
														}catch (Exception e){
															log.error(e.getMessage(),e);
														}
														log.info("超时小时数："+chaoshiHours);
														log.info("最大通知次数："+maxAlarmTimes.intValue());
														if(1==maxAlarmTimes.intValue()){
															if(warnTime.compareTo(new Date())<0){

																warnTimes=1;//提醒次数
															}

														}else if(1<maxAlarmTimes.intValue()){
															BigDecimal integerPartInterval = intervalHour.setScale(0, RoundingMode.DOWN);
															BigDecimal fractionalPartInterval = intervalHour.subtract(integerPartInterval);
															int hourValueInterval=integerPartInterval.intValue();
															int minuteValueInterval=fractionalPartInterval.multiply(new BigDecimal("60")).intValue();
															//重复多次
															boolean mark=true;
															Date tmpTime=calendar.getTime();
															if(warnTime.compareTo(new Date())<0){

																warnTimes=1;//提醒次数
															}
															if(0!=hourValueInterval||0!=minuteValueInterval){
																while(mark){
																	calendar.add(Calendar.HOUR_OF_DAY,hourValueInterval);
																	calendar.add(Calendar.MINUTE,minuteValueInterval);
																	tmpTime=calendar.getTime();
																	if(calendar.getTime().compareTo(new Date())>0){
																		mark=false;
																	}else{
																		warnTimes++;
																	}

																}
															}


														}
														log.info("本次通知次数："+warnTimes);

														if(warnTimes>0&&warnTimes<=maxAlarmTimes.intValue()){
															QueryWrapper<DingMsg> dingMsgQueryWrapper=new QueryWrapper<>();
															dingMsgQueryWrapper.eq("ding_msg_type",WorkConst.DING_MSG_TYPE_DEADLINE);
															dingMsgQueryWrapper.eq("work_order",workOrder.getId());
															dingMsgQueryWrapper.eq("work_order_status",workOrder.getStatus());
															dingMsgQueryWrapper.eq("warn_times",warnTimes);
															DingMsg dingMsg=dingMsgService.getOne(dingMsgQueryWrapper);
															String msgType=WorkConst.MSG_TYPE_DEADLINE;
															if(null==dingMsg){
																try {
																	log.info("开始发送消息");
																	if("1".equals(workOrderType.getDuplyMsgToAlignUser())&workOrder.getStatus().compareTo(WorkConst.ORDER_STATUS_ALIGN)>0){
																		new SendMsgThread(workOrder.getId(),WorkConst.MSG_TYPE_DEADLINE_CHAOSONGPAIDAN,domain,agentId,appKey,appSecret,workOrderService,sysBaseAPI,workOrderTypeUserService,dingMsgService
																				,workOrderTypeDiyColumnService,chaoshiHours.intValue()
																				,WorkConst.DING_MSG_TYPE_DEADLINE_TOALIGN,warnTimes).start();
																	}

																	new SendMsgThread(workOrder.getId(),msgType,domain,agentId,appKey,appSecret,workOrderService,sysBaseAPI,workOrderTypeUserService,dingMsgService
																			,workOrderTypeDiyColumnService,chaoshiHours.intValue()
																			,WorkConst.DING_MSG_TYPE_DEADLINE,warnTimes).start();
																}catch (Exception e){
																	log.error(e.getMessage(),e);
																}
															}
														}else{
															log.info("本次通知次数0次不通知");
														}


													}else{
														log.info("beginHour或statusTime或maxAlarmTimes为空");
													}

												}else{
													log.info("类型未匹配");
												}
											}else{
												log.info("类型未匹配");
											}
										}else{
											log.info("超时配置类型状态为匹配");
										}
									}else{
										log.info("系统无超时配置");
									}

									//自动接单和超时自动确认
									String type=workOrder.getType();
									Optional<WorkOrderType> orderOptional= workOrderTypes.stream().filter(item->item.getId().equals(type)).findFirst();
									if(null!=orderOptional&&orderOptional.isPresent()){

										WorkOrderType workOrderType=orderOptional.get();

										if(null!=workOrderType){
											//自动接单
											if(WorkConst.ORDER_STATUS_GET.equals(workOrder.getStatus())){

												//自动接单逻辑
												String autoGet=workOrderType.getAutoGet();
												if("1".equals(autoGet)){
													workOrder.setStatus(WorkConst.ORDER_STATUS_HANDLE);
													workOrder.setGetTime(new Date());
													workOrderService.updateById(workOrder);

													WorkOrderRealign workOrderRealign=new WorkOrderRealign();
													workOrderRealign.setWorkOrder(workOrder.getId());
													workOrderRealign.setType(WorkConst.PAIDAN_STATUS_GET);
													workOrderRealign.setToUser(workOrder.getToUser());
													workOrderRealign.setAlignTime(new Date());
													String json=JSONObject.toJSONString(workOrder);
													workOrderRealign.setJson(json);
													workOrderRealign.setRealignReason("系统自动接单");
													workOrderRealign.setBeginTime(workOrder.getAlignTime());
													workOrderRealignService.save(workOrderRealign);
													log.info("系统自动接单：");
													continue;
												}

											}
											if(WorkConst.ORDER_STATUS_SCORE.equals(workOrder.getStatus())){
												//超时自动确认逻辑
												String autoQueren=workOrderType.getAutoQueren();
												BigDecimal autoQuerenHours=workOrderType.getAutoQuerenHours();
												if("1".equals(autoQueren)){
													if(null!=autoQuerenHours){
														BigDecimal integerPartEnd = autoQuerenHours.setScale(0, RoundingMode.DOWN);
														BigDecimal fractionalPartEnd = autoQuerenHours.subtract(integerPartEnd);
														int hourValueEnd=integerPartEnd.intValue();
														int minuteValueEnd=fractionalPartEnd.multiply(new BigDecimal("60")).intValue();
														log.info("自动确认小时分钟数："+hourValueEnd+"——"+minuteValueEnd);
														Calendar calendarEnd=Calendar.getInstance();
														calendarEnd.setTime(workOrder.getSolveTime());
														if(0!=hourValueEnd){
															calendarEnd.add(Calendar.HOUR_OF_DAY,hourValueEnd);
														}
														if(0!=minuteValueEnd){
															calendarEnd.add(Calendar.MINUTE,minuteValueEnd);
														}
														Date daoqiTime=calendarEnd.getTime();//到期时间
														if(new Date().compareTo(daoqiTime)>=0){
															workOrder.setStatus(WorkConst.ORDER_STATUS_DONE);
															workOrder.setScoreUser("系统自动确认");
															workOrder.setScoreUserSatisfied("1");
															workOrder.setScoreTime(new Date());
//															workOrder.setScoreUserOpinion("系统自动确认");
															workOrderService.updateById(workOrder);

															WorkOrderRealign workOrderRealign=new WorkOrderRealign();
															workOrderRealign.setWorkOrder(workOrder.getId());
															workOrderRealign.setType(WorkConst.PAIDAN_STATUS_PINGJIA);
															workOrderRealign.setRealignReason("系统自动确认");
															workOrderRealign.setFromUser("系统自动确认");
															workOrderRealign.setAlignTime(new Date());
															workOrderRealign.setRealignReason(workOrder.getIsSolveVerifyRemark());
															String json=JSONObject.toJSONString(workOrder);
															workOrderRealign.setJson(json);
															workOrderRealign.setBeginTime(workOrder.getScoreTime());
															workOrderRealignService.save(workOrderRealign);

															log.info("系统自动确认：");
															continue;
														}else{
															log.info("自动确认尚未超时");
														}

													}

												}

											}



										}else{
											log.info("类型未匹配");
										}
									}else{
										log.info("类型未匹配");
									}


								}catch (Exception e){
									log.error(e.getMessage(),e);
								}

							}
						}
					}catch (Exception e){
						log.error(e.getMessage(),e);
					}

					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
//						e.printStackTrace();
						log.error(e.getMessage(),e);
					}

				}
			}
		}

	public static long dateDiffHour(Date startTime, Date endTime) throws Exception {
		//按照传入的格式生成一个simpledateformate对象
		long nd = 1000*24*60*60;//一天的毫秒数
		long nh = 1000*60*60;//一小时的毫秒数
		long nm = 1000*60;//一分钟的毫秒数
		long ns = 1000;//一秒钟的毫秒数
		long diff;
		//获得两个时间的毫秒时间差异
		diff = endTime.getTime() - startTime.getTime();
//		long day = diff/nd;//计算差多少天
		long hour = diff/nh;//计算差多少小时
//		long min = diff%nd%nh/nm;//计算差多少分钟
//		long sec = diff%nd%nh%nm/ns;//计算差多少秒//输出结果
//		System.out.println("时间相差："+day+"天"+hour+"小时"+min+"分钟"+sec+"秒。");
		return hour ;
	}
	public static  String  timeConsuming(Date startTime, Date endTime) {
		if(null==startTime||null==endTime){
			return "";
		}
		//按照传入的格式生成一个simpledateformate对象
		long nd = 1000*24*60*60;//一天的毫秒数
		long nh = 1000*60*60;//一小时的毫秒数
		long nm = 1000*60;//一分钟的毫秒数
		long ns = 1000;//一秒钟的毫秒数
		long diff;
		//获得两个时间的毫秒时间差异
		diff = endTime.getTime() - startTime.getTime();
		long day = diff/nd;//计算差多少天
		long hour = diff%nd/nh;//计算差多少小时
		long min = diff%nd%nh/nm;//计算差多少分钟
		long sec = diff%nd%nh%nm/ns;//计算差多少秒//输出结果
		String dayStr=day<=0l?"":(day+"天");
		String hourStr=hour<=0l?"":(hour+"小时");
		String minStr=min<=0l?"":(min+"分钟");
		String secStr=sec<=0l?"":(min+"秒");
		String rel=dayStr+hourStr+minStr+secStr;
		return rel ;
	}

	public static void main(String[] args) {
		String begin="2022-09-08 22:14:23";
		String end="2022-09-10 02:14:23";
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String aa= null;
		try {
			aa = timeConsuming(sdf.parse(begin),sdf.parse(end));
		} catch (ParseException e) {
			e.printStackTrace();
		};
		System.out.println(aa);
	}

}
