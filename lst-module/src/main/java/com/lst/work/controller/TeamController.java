package com.lst.work.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lst.work.entity.*;
import com.lst.work.mapper.WorkOrderMapper;
import com.lst.work.service.ITeamUserService;
import com.lst.work.service.IWorkOrderTypeTeamService;
import com.lst.work.service.IWorkOrderTypeUserService;
import io.netty.util.internal.StringUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.CommonUtils;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.service.ITeamService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 团队
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Api(tags="团队")
@RestController
@RequestMapping("/work/team")
@Slf4j
public class TeamController extends JeecgController<Team, ITeamService> {
	@Autowired
	private ITeamService teamService;
	@Autowired
	private ITeamUserService teamUserService;
	 @Autowired
	 private ISysBaseAPI sysBaseAPI;
	 @Autowired
	 private WorkOrderMapper workOrderMapper;
	 @Autowired
	 private IWorkOrderTypeTeamService workOrderTypeTeamService;

	 @Autowired
	 private IWorkOrderTypeUserService workOrderTypeUserService;
	/**
	 * 分页列表查询
	 *
	 * @param team
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "团队-分页列表查询")
	@ApiOperation(value="团队-分页列表查询", notes="团队-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(Team team,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Team> queryWrapper = QueryGenerator.initQueryWrapper(team, req.getParameterMap());
		Page<Team> page = new Page<Team>(pageNo, pageSize);
		IPage<Team> pageList = teamService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	 @ApiOperation(value="团队-分页列表查询", notes="团队-分页列表查询")
	 @GetMapping(value = "/listAll")
	 public Result<?> listAll(Team team,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 QueryWrapper<Team> queryWrapper = QueryGenerator.initQueryWrapper(team, req.getParameterMap());
		 Page<Team> page = new Page<Team>(pageNo, pageSize);
		 IPage<Team> pageListTmp = teamService.page(page, queryWrapper);




		 QueryWrapper<TeamUser> workOrderTypeTeamQueryWrapper=new QueryWrapper<>();
		 //workOrderTypeTeamQueryWrapper.eq("tenant_id",team.getTenantId());
		 List<TeamUser> teamUsers=teamUserService.list();
		 List<JSONObject> teamUsersJson=sysBaseAPI.parseDictTextList(teamUsers);

		 IPage<TeamView> pageList=new Page<TeamView>();
		 List<TeamView> teamViews=new ArrayList<>();
		 if(null!=pageListTmp&&null!=pageListTmp.getRecords()&&pageListTmp.getRecords().size()>0){
			 teamViews = CommonUtils.copyListProperties(pageListTmp.getRecords(), TeamView.class);

			 for(TeamView v:teamViews){
				 List<String> users=new ArrayList<String>();
				 List<JSONObject> alignUserObj=new ArrayList<JSONObject>();
				 if(null!=teamUsersJson&&teamUsersJson.size()>0){
					 for(JSONObject teamUser:teamUsersJson){
						 if(v.getId().equals(teamUser.get("team"))){
							 users.add(null!=teamUser.get("user")?(String)teamUser.get("user"):"");
						 }
					 }
				 }
				 v.setUsers(users);
			 }
		 }
		 pageList.setCurrent(pageListTmp.getCurrent());
		 pageList.setPages(pageListTmp.getPages());
		 pageList.setSize(pageListTmp.getSize());
		 pageList.setTotal(pageListTmp.getTotal());
		 pageList.setRecords(teamViews);
		 return Result.ok(pageList);

	 }
	 ////@AutoLog(value = "团队用户查询")
	 @ApiOperation(value="团队用户查询", notes="团队用户查询")
	 @GetMapping(value = "/getTeamUsers")
	 public Result<?> getTeamUsers(Team team,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 QueryWrapper<Team> queryWrapper = QueryGenerator.initQueryWrapper(team, req.getParameterMap());
		 Page<Team> page = new Page<Team>(pageNo, pageSize);
		 IPage<Team> pageList = teamService.page(page, queryWrapper);
		 List<JSONObject> jsonObjects =new ArrayList<JSONObject>();

		 List<HashMap<String,Object>> rel=workOrderMapper.countDaiban();
//		 if(null!=rel&& rel.size()>0){
//			 Long count=(Long)rel.get(0).get("count");
//			 workOrder.setDuplicated60(count.intValue());
//		 }
		 if(null!=pageList&&null!=pageList.getRecords()&&pageList.getRecords().size()>0){
			 QueryWrapper<TeamUser> teamUserQueryWrapper=new QueryWrapper<TeamUser>();
			 List<TeamUser> teamUsersL=teamUserService.list();
			 if(null!=teamUsersL&&teamUsersL.size()>0){
				 List<JSONObject> teamUsers=sysBaseAPI.parseDictTextList(teamUsersL);
				 for(Team t:pageList.getRecords()){
					 JSONObject jsonObject=new JSONObject();
					 jsonObject.put("value",t.getId());
					 jsonObject.put("label",t.getName());
//					 jsonObject.put("disabled","true");
					 if(null!=teamUsers&&teamUsers.size()>0){
						 List<JSONObject> temp=new ArrayList<JSONObject>();
						 for(JSONObject tu:teamUsers){
							 if(t.getId().equals(tu.get("team"))){
								 JSONObject inner=new JSONObject();
								 inner.put("value",tu.get("user"));
								 inner.put("label",tu.get("user_dictText"));
								 if(null!=rel&&rel.size()>0){
									 Optional<HashMap<String,Object>>  selected=rel.stream().filter(item->((String)item.get("to_user")).equals(tu.get("user"))).findFirst();
									 if(null!=selected&&selected.isPresent()){
										 Long count=(Long)selected.get().get("count");
										 inner.put("label",tu.get("user_dictText")+"(待处理:"+count.intValue()+")");
									 }
								 }


								 temp.add(inner);
							 }
						 }
						 jsonObject.put("children",temp);
					 }
					 jsonObjects.add(jsonObject);

				 }
			 }

		 }

		 return Result.ok(jsonObjects);
	 }
	 @ApiOperation(value="团队-分页列表查询", notes="团队-分页列表查询")
	 @GetMapping(value = "/getAllTeam")
	 public Result<?> getAllTeam(Team team,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 QueryWrapper<Team> queryWrapper = QueryGenerator.initQueryWrapper(team, req.getParameterMap());
		 Page<Team> page = new Page<Team>(pageNo, pageSize);
		 IPage<Team> pageList = teamService.page(page, queryWrapper);
		 List<JSONObject> jsonObjects =new ArrayList<JSONObject>();
		if(null!=pageList&&null!=pageList.getRecords()&&pageList.getRecords().size()>0){
			List<JSONObject> teams=sysBaseAPI.parseDictTextList(pageList.getRecords());
			for(Team t:pageList.getRecords()){
				JSONObject jsonObject=new JSONObject();
				jsonObject.put("value",t.getId());
				jsonObject.put("label",t.getName());

				jsonObjects.add(jsonObject);

			}
		}


		 return Result.ok(jsonObjects);
	 }
	 ////@AutoLog(value = "所有用户查询")
	 @ApiOperation(value="所有用户查询", notes="所有用户查询")
	 @GetMapping(value = "/getAllUsers")
	 public Result<?> getAllUsers(Team team,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		 List<JSONObject> allUsers=sysBaseAPI.getAllUserNames();
		 String workOrderType=req.getParameter("workOrderType");
		 if(StringUtil.isNullOrEmpty(workOrderType)){
		 	return Result.ok(allUsers);
		 }
		 List<HashMap<String,Object>> rel=workOrderMapper.countDaiban();
		 QueryWrapper<WorkOrderTypeTeam> workOrderTypeTeamQueryWrapper= new QueryWrapper<WorkOrderTypeTeam>();
		 workOrderTypeTeamQueryWrapper.eq("work_order_type",workOrderType);
		List <WorkOrderTypeTeam> workOrderTypeTeams=workOrderTypeTeamService.list(workOrderTypeTeamQueryWrapper);
		 List<JSONObject> temp=new ArrayList<JSONObject>();
		if(null!=workOrderTypeTeams&&workOrderTypeTeams.size()>0){
			String teamId=workOrderTypeTeams.get(0).getTeam();
			if(!StringUtil.isNullOrEmpty(teamId)){


			 QueryWrapper<TeamUser> teamUserQueryWrapper=new QueryWrapper<TeamUser>();
			 teamUserQueryWrapper.eq("team",teamId);
			 List<TeamUser> teamUsersL=teamUserService.list(teamUserQueryWrapper);
			 if(null!=teamUsersL&&teamUsersL.size()>0){
				 List<JSONObject> teamUsers=sysBaseAPI.parseDictTextList(teamUsersL);
					 if(null!=teamUsers&&teamUsers.size()>0){

						 for(JSONObject tu:teamUsers){
								 JSONObject inner=new JSONObject();
								 inner.put("value",tu.get("user"));
								 inner.put("label",tu.get("user_dictText"));
								 if(null!=rel&&rel.size()>0){
									 Optional<HashMap<String,Object>>  selected=rel.stream().filter(item->((String)item.get("to_user")).equals(tu.get("user"))).findFirst();
									 if(null!=selected&&selected.isPresent()){
										 Long count=(Long)selected.get().get("count");
										 inner.put("label",tu.get("user_dictText")+"(待处理:"+count.intValue()+")");
									 }
								 }


								 temp.add(inner);
							 }
						 }
					 }
				 }
			 }

		return Result.ok(temp);
	 }

	/**
	 *   添加
	 *
	 * @param team
	 * @return
	 */
	//@AutoLog(value = "团队-添加")
	@ApiOperation(value="团队-添加", notes="团队-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody Team team) {
		teamService.save(team);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param team
	 * @return
	 */
	//@AutoLog(value = "团队-编辑")
	@ApiOperation(value="团队-编辑", notes="团队-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody Team team) {
		teamService.updateById(team);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "团队-通过id删除")
	@ApiOperation(value="团队-通过id删除", notes="团队-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		teamService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	//@AutoLog(value = "团队-批量删除")
	@ApiOperation(value="团队-批量删除", notes="团队-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.teamService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "团队-通过id查询")
	@ApiOperation(value="团队-通过id查询", notes="团队-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		Team team = teamService.getById(id);
		if(team==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(team);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param team
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Team team) {
        return super.exportXls(request, team, Team.class, "团队");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Team.class);
    }

}
