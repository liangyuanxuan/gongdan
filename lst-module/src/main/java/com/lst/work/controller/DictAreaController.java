package com.lst.work.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.lst.work.entity.SysSetting;
import com.lst.work.service.ISysSettingService;
import com.lst.work.util.WorkConst;
import io.netty.util.internal.StringUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.DictArea;
import com.lst.work.service.IDictAreaService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 地区
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
@Api(tags="地区")
@RestController
@RequestMapping("/work/dictArea")
@Slf4j
public class DictAreaController extends JeecgController<DictArea, IDictAreaService>{
	@Autowired
	private IDictAreaService dictAreaService;
	@Autowired
	private ISysSettingService sysSettingService;
	
	/**
	 * 分页列表查询
	 *
	 * @param dictArea
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "地区-分页列表查询")
	@ApiOperation(value="地区-分页列表查询", notes="地区-分页列表查询")
	@GetMapping(value = "/rootList")
	public Result<?> queryPageList(DictArea dictArea,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String parentId = dictArea.getPid();
		if (oConvertUtils.isEmpty(parentId)) {
			parentId = "0";
		}
		dictArea.setPid(null);
		QueryWrapper<DictArea> queryWrapper = QueryGenerator.initQueryWrapper(dictArea, req.getParameterMap());
		// 使用 eq 防止模糊查询
		queryWrapper.eq("pid", parentId);
		Page<DictArea> page = new Page<DictArea>(pageNo, pageSize);
		IPage<DictArea> pageList = dictAreaService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	 /**
      * 获取子数据
      * @param req
      * @return
      */
	@AutoLog(value = "地区-获取子数据")
	@ApiOperation(value="地区-获取子数据", notes="地区-获取子数据")
	@GetMapping(value = "/childList")
	public Result<?> queryPageList(DictArea dictArea,HttpServletRequest req) {
		QueryWrapper<DictArea> queryWrapper = QueryGenerator.initQueryWrapper(dictArea, req.getParameterMap());
		List<DictArea> list = dictAreaService.list(queryWrapper);
		return Result.ok(list);
	}
	 /**
	  * 获取子数据
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "获取地区字典值")
	 @ApiOperation(value="地区-获取子数据", notes="地区-获取子数据")
	 @GetMapping(value = "/getRegionDict")
	 public Result<?> getRegionDict(HttpServletRequest req) {

		 QueryWrapper<SysSetting> sysSettingQueryWrapper=new QueryWrapper<SysSetting>();
		 sysSettingQueryWrapper.eq("code", WorkConst.SYS_SETTING_ROOTREGION);
		 SysSetting sysSetting = sysSettingService.getOne(sysSettingQueryWrapper);
		 if(null!=sysSetting){
		 	String value=sysSetting.getValue();
		 	if(!StringUtil.isNullOrEmpty(value)){
				QueryWrapper<DictArea> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("code",value);
				DictArea root=dictAreaService.getOne(queryWrapper);

				QueryWrapper<DictArea> queryWrapperPar = new QueryWrapper<>();
				queryWrapperPar.eq("pid",value);
				List<DictArea> dictAreas=dictAreaService.list(queryWrapper);
//				if()
			}
		 }
		 List<JSONObject> jsonObjects=new ArrayList<>();
		 return Result.ok(jsonObjects);
	 }
	
	
	/**
	 *   添加
	 *
	 * @param dictArea
	 * @return
	 */
	@AutoLog(value = "地区-添加")
	@ApiOperation(value="地区-添加", notes="地区-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody DictArea dictArea) {
		dictAreaService.addDictArea(dictArea);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param dictArea
	 * @return
	 */
	@AutoLog(value = "地区-编辑")
	@ApiOperation(value="地区-编辑", notes="地区-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody DictArea dictArea) {
		dictAreaService.updateDictArea(dictArea);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "地区-通过id删除")
	@ApiOperation(value="地区-通过id删除", notes="地区-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		dictAreaService.deleteDictArea(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "地区-批量删除")
	@ApiOperation(value="地区-批量删除", notes="地区-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.dictAreaService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "地区-通过id查询")
	@ApiOperation(value="地区-通过id查询", notes="地区-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		DictArea dictArea = dictAreaService.getById(id);
		if(dictArea==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(dictArea);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param dictArea
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, DictArea dictArea) {
		return super.exportXls(request, dictArea, DictArea.class, "地区");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, DictArea.class);
    }

}
