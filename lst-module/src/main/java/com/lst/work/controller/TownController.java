package com.lst.work.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.Town;
import com.lst.work.service.ITownService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 镇
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
@Api(tags="镇")
@RestController
@RequestMapping("/work/town")
@Slf4j
public class TownController extends JeecgController<Town, ITownService> {
	@Autowired
	private ITownService townService;
	
	/**
	 * 分页列表查询
	 *
	 * @param town
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "镇-分页列表查询")
	@ApiOperation(value="镇-分页列表查询", notes="镇-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(Town town,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Town> queryWrapper = QueryGenerator.initQueryWrapper(town, req.getParameterMap());
		Page<Town> page = new Page<Town>(pageNo, pageSize);
		IPage<Town> pageList = townService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param town
	 * @return
	 */
	@AutoLog(value = "镇-添加")
	@ApiOperation(value="镇-添加", notes="镇-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody Town town) {
		townService.save(town);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param town
	 * @return
	 */
	@AutoLog(value = "镇-编辑")
	@ApiOperation(value="镇-编辑", notes="镇-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody Town town) {
		townService.updateById(town);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "镇-通过id删除")
	@ApiOperation(value="镇-通过id删除", notes="镇-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		townService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "镇-批量删除")
	@ApiOperation(value="镇-批量删除", notes="镇-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.townService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "镇-通过id查询")
	@ApiOperation(value="镇-通过id查询", notes="镇-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		Town town = townService.getById(id);
		if(town==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(town);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param town
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Town town) {
        return super.exportXls(request, town, Town.class, "镇");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Town.class);
    }

}
