package com.lst.work.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.netty.util.internal.StringUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.ProblemType;
import com.lst.work.service.IProblemTypeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 问题类型
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Api(tags="问题类型")
@RestController
@RequestMapping("/work/problemType")
@Slf4j
public class ProblemTypeController extends JeecgController<ProblemType, IProblemTypeService> {
	@Autowired
	private IProblemTypeService problemTypeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param problemType
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "问题类型-分页列表查询")
	@ApiOperation(value="问题类型-分页列表查询", notes="问题类型-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ProblemType problemType,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String aa=problemType.getName();
		if(!StringUtil.isNullOrEmpty(aa)){
			problemType.setName(null);
		}
		QueryWrapper<ProblemType> queryWrapper = QueryGenerator.initQueryWrapper(problemType, req.getParameterMap());
		if(!StringUtil.isNullOrEmpty(aa)){
			queryWrapper.like("name",aa);
		}
		Page<ProblemType> page = new Page<ProblemType>(pageNo, pageSize);
		IPage<ProblemType> pageList = problemTypeService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param problemType
	 * @return
	 */
	//@AutoLog(value = "问题类型-添加")
	@ApiOperation(value="问题类型-添加", notes="问题类型-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ProblemType problemType) {
		problemTypeService.save(problemType);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param problemType
	 * @return
	 */
	//@AutoLog(value = "问题类型-编辑")
	@ApiOperation(value="问题类型-编辑", notes="问题类型-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ProblemType problemType) {
		problemTypeService.updateById(problemType);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "问题类型-通过id删除")
	@ApiOperation(value="问题类型-通过id删除", notes="问题类型-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		problemTypeService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	//@AutoLog(value = "问题类型-批量删除")
	@ApiOperation(value="问题类型-批量删除", notes="问题类型-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.problemTypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "问题类型-通过id查询")
	@ApiOperation(value="问题类型-通过id查询", notes="问题类型-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ProblemType problemType = problemTypeService.getById(id);
		if(problemType==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(problemType);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param problemType
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ProblemType problemType) {
        return super.exportXls(request, problemType, ProblemType.class, "问题类型");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ProblemType.class);
    }

}
